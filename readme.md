[![made-with-datalad](https://www.datalad.org/badges/made_with.svg)](https://datalad.org)

## EEG Task preprocessing

**Maintainer:** [Julian Q. Kosciessa](kosciessa@mpib-berlin.mpg.de)

Preprocessing and analysis of EEG data were conducted with the FieldTrip toolbox (v.20170904) 79 and using custom-written MATLAB (The MathWorks Inc., Natick, MA, USA) code. Offline, EEG data were filtered using a 4th order Butterworth filter with a pass-band of 0.5 to 100 Hz. Subsequently, data were downsampled to 500 Hz and all channels were re-referenced to mathematically averaged mastoids. Blink, movement and heart-beat artifacts were identified using Independent Component Analysis (ICA; 80) and removed from the signal. Artifact-contaminated channels (determined across epochs) were automatically detected using (a) the FASTER algorithm 81, and by (b) detecting outliers exceeding three standard deviations of the kurtosis of the distribution of power values in each epoch within low (0.2-2 Hz) or high (30-100 Hz) frequency bands, respectively. Rejected channels were interpolated using spherical splines 82. Subsequently, noisy epochs were likewise excluded based on a custom implementation of FASTER and on recursive outlier detection. Finally, recordings were segmented to participant cues to open their eyes and were epoched into non-overlapping 3 second pseudo-trials. To enhance spatial specificity, scalp current density estimates were derived via 4th order spherical splines 82 using a standard 1005 channel layout (conductivity: 0.33 S/m; regularization: 1^-05; 14th degree polynomials).

-----

Most of the steps will be conducted using FieldTrip, except for the initial step that is done in eeglab to profit from the eyelab toolbox by Olaf Dimigen for alignment of EEG and ET data.

Starting at script C_* there is basically a branch resulting in the ICAs. The final data branch starts at script G_*, where we start from the raw data again and then apply the filter settings we require for the analyses. The ICA components will then be removed from the latter data.

```
% N = 48 YAs;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';'1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1213';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};

% N = 53 OAs;
IDs = {'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';'2252';'2258';'2261'};
```

**0_copyRawEEG_study**

* Copy raw EEG data (combined task, rest and stroop)

**A_STSW_ConvertASC2MAT_20171221**

* Convert Eyelink ASCII files to mat format using eeglab
* The creation of ASCII files is done in the `eye` folder.

**A2_replaceETTriggers**

* As for ~80% of recordings, no ET triggers were available, I reconstructed the task design triggers for ET via the messages that were simultaneous with EEG triggers, I did this for ALL subjects to remove the change of differential error. [see note below for more info]

**B_STSW_ImportEyeEEG_180103**

* Split EEG data into runs, combine EEG-ET data (eeglab toolbox)

**C_STSW_prepare_preprocessing_180111**

* Prepare for ICA
* Read into FieldTrip format
* Switch channels
* EEG settings:
  * Referenced to avg. mastoid (A1, A2)
  * downsample: 1000Hz to 250 Hz
  * 4th order Butterworth 1-100 Hz BPF
  * no reref for ECG

**D_STSW_visual_inspection_for_ica_180105**

* Check for gross noise periods that should not be considered for ICA

**D2_STSW_carryOverVisualInspection**

* If a previous step has to be redone, we want to carry over the indications about visually identified noise periods to the new config-structures

This had to be run as we needed to switch the channels (C_...) after originally keeping the original channel setup.

**D3_correctMatrices_v2**

* This script is a hack to deal with a previous script problem: During visual inspection, data may not have cleared from previous participants, thereby carrying over their labels of artifact periods. This script attempts to fix this problem by retaining only periods until the offset of the measured interval. A visual check indicated that this fixed the issue.

**D5_InspectChannelArrangement**

* Check whether the correlational structure of the channels looks sensible. If not, there may have been problems with electrode placement.

**E_STSW_ica1_180108**

* Conduct initial ICA1, this should be run on tardis

**F_STSW_ICA_labeling_180111**

* Manual labeling of artefactual ICA components

**G_STSW_segmentation_raw_data_180111**

* Segmentation: -1500 ms relative to fixcue onset to 1500 ms after ITI onset
* Load raw data
* Switch channels
* EEG settings:
  * Referenced to avg. mastoid (A1, A2)
  * 0.2 4th order butterworth HPF
  * 125 4th order butterworth LPF
  * demean
  * recover implicit reference: A2
  * downsample: 1000Hz to 500 Hz
* Note that for Subject 1223 (runs 2,3,4) and 1228 (run 4), the script had to be fixed post-hoc as no ET data are available. The script `…_noTardis` was run on these repeat subjects. The same conditional has since been introduced into the main script, but it has not been recompiled.
* For subjects 2160 and 2203 the initial block onset triggers are missing. Dummy triggers are placed at the first events, but care should be taken in further preprocessing.

**H_STSW_automatic_artifact_correction_20170922**

* Automatic artifact correction, interpolation
* Remove blink, move, heart, ref, art & emg ICA components prior to calculation
* get artifact contaminated channels by kurtosis, low & high frequency artifacts
* get artifact contaminated channels by FASTER
* interpolate artifact contaminated channels
* equalize duration of trials to the trial with fewest samples
o This is a hack. Apparently, we did not always get consistent timing on each trial, such that some trials were a bit shorter. To include these trials, I cut every trial to the lowest trial length.
o Interpolation to a general length may be the better option.
* get artifact contaminated epochs & exclude epochs recursively
* get channel x epoch artifacts
* Note that this does NOT yet remove anything. We only calculate the data to be removed in the next setp (I).

**I_STSW_prep_data_for_analysis_20170922**

* Remove blink, move, heart, ref, art & emg ICA components
* Interpolate detected artifact channels
* Remove artifact-heavy trials, for subjects with missing onsets, the missing trials are included here as `artefactual trials`, hence correcting the EEG-behavior assignment:

% IMPORTANT: For 2160, the first two trials were not available,
% hence we have to add +2 to the index to identify the correct
% behavioral trials that belong to the EEG data. For 2203 the
% first trial is missing. The third and second trial should
% also be removed, regardless of whether they are indicated to
% be dropped due to automatic correction.

**J_STSW_assignConditionsToData_170927**

* Remove additional channels
* Load behavioral data and add information to data

**K_copyFinalPreprocToMainStruct**

* Create a superdirectory on tardis and copy the final preproc data for further analyses
* Note: There were some corrupted target data after copying. Manual transfer of those files was successful.

## General Notes

IMPORTANT: For the actual study, 80% of eye tracking recordings (YAs) did NOT receive the proper trigger.

The novel script `A2_replaceETTriggers` creates a matching trigger representation based on the ET events. As not all triggers have corresponding ET events, the extraction range also has been changed from the pilot settings to cover the run onset to the final ITI onset. Note that this is done for ALL datasets, even if triggers were recorded to keep the error constant across the dataset.

Note that a different cable (borrowed from ConMem) has been used for sending triggers during the pilot acquisition.

## Processing notes

* 1167 EEG was labeled as 1267 throughout. This has been changed manually.
* 1167 stroop1 EEG had a `t` appended. This has been changed manually.
* 1216 EEG were all named `s_XXXX`, `dynamics` was `dynamics`. Manually changed.
* 1245 EEG was labeled as 1234 as established by the folder and timestamps. Changed.
* subject 1228 had only the first 3 runs recorded (ET + EEG)
* ss_2160_dynamict.eeg manually renamed to ss_2160_dynamic.eeg

Note that the channels A1 and FCz were generally exchanged in the BrainVisionRecorder. During some sessions, it was also apparent that the amplifiers had been switched. These channels have been switched based on visual inspection of the expected autocorrelation of neighboring channels (see C_figures/ C_ChannelCheck for the correlative structure afterwards). Note that this switching is only done AFTER the data are read into FieldTrip, i.e. the eeglab data have the WRONG channel assignment! The scripts that do the switching can be found in the directory A_scripts/helper/. C_STSW_prepare_preprocessing_180111 contains the conditionals for what is switched in which subject. Visual inspection of ICAs indicated that the channel switch was successful, although outlier channels may still be slightly off.

Following the final step (adding condition info to the data), preprocessed data were copied to the superdirectory X1_preprocEEGData, so that other processing functions can immediately work with them and the preproc step can be deleted from the cluster.

For subject 2112, channels Fz and FCz were interpolated prior to running the ICA. This was done as originally, the channels pervaded all ICA components. Subjects’ 2129, 2131, 2133, 2222 and 2224 ICAs had to be rerun as there were corruptions to the original data.

## Original ConMem EEG-ET pipeline

* Convert ET data into asci (in eye folder)
* Convert ET data into .mat (in EEG folder as eeglab is used)
* Split EEG data into runs, combine EEG-ET data (eeglab toolbox)
* Prepare for ICA
  * Reref (REF(A1)+A2, A2 as implicit ref), 250 Hz downsampling, 4th order Butterworth 1-100 Hz BPF
  * (48-52 Hz bandstop filter, not applied for now)
  * no reref for ECG
* Visual inspection for ICA
* Implement ET data on blinks + fixations to identify ICA components
* ICA1
* Segmentation
* Automatic artifact correction, interpolation
* IC2

ET processing by MD:

* Downsampling to 250Hz, segmentation
* Automatic masking of blinks and half blinks (detectable from velocity and pupil time series)
* Automatic pre-labeling in FieldTrip
* Remove short time windows around blinks as well
* Imputation of blinks

Outputs:

* C_prepare_preproc: `_EEG_Raw_Rlm_Flh_Res.mat`
* E_ica: `_EEG_Rlm_Fhl_Ica.mat`
* G_segmentFromRaw: `_EEG_Rlm_Fhl_rdSeg.mat`
* I_artCorrData: `_EEG_Rlm_Fhl_rdSeg_Art.mat`
* J_ArtCorrWithCond: `_EEG_Rlm_Fhl_rdSeg_Art_Cond.mat`

---

# **DataLad datasets and how to use them**

This repository is a [DataLad](https://www.datalad.org/) dataset. It provides
fine-grained data access down to the level of individual files, and allows for
tracking future updates. In order to use this repository for data retrieval,
[DataLad](https://www.datalad.org/) is required. It is a free and
open source command line tool, available for all major operating
systems, and builds up on Git and [git-annex](https://git-annex.branchable.com/)
to allow sharing, synchronizing, and version controlling collections of
large files. You can find information on how to install DataLad at
[handbook.datalad.org/en/latest/intro/installation.html](http://handbook.datalad.org/en/latest/intro/installation.html).

### Get the dataset

A DataLad dataset can be `cloned` by running

```
datalad clone <url>
```

Once a dataset is cloned, it is a light-weight directory on your local machine.
At this point, it contains only small metadata and information on the
identity of the files in the dataset, but not actual *content* of the
(sometimes large) data files.

### Retrieve dataset content

After cloning a dataset, you can retrieve file contents by running

```
datalad get <path/to/directory/or/file>`
```

This command will trigger a download of the files, directories, or
subdatasets you have specified.

DataLad datasets can contain other datasets, so called *subdatasets*.
If you clone the top-level dataset, subdatasets do not yet contain
metadata and information on the identity of files, but appear to be
empty directories. In order to retrieve file availability metadata in
subdatasets, run

```
datalad get -n <path/to/subdataset>
```

Afterwards, you can browse the retrieved metadata to find out about
subdataset contents, and retrieve individual files with `datalad get`.
If you use `datalad get <path/to/subdataset>`, all contents of the
subdataset will be downloaded at once.

### Stay up-to-date

DataLad datasets can be updated. The command `datalad update` will
*fetch* updates and store them on a different branch (by default
`remotes/origin/master`). Running

```
datalad update --merge
```

will *pull* available updates and integrate them in one go.

### Find out what has been done

DataLad datasets contain their history in the ``git log``.
By running ``git log`` (or a tool that displays Git history) in the dataset or on
specific files, you can find out what has been done to the dataset or to individual files
by whom, and when.

### More information

More information on DataLad and how to use it can be found in the DataLad Handbook at
[handbook.datalad.org](http://handbook.datalad.org/en/latest/index.html). The chapter
"DataLad datasets" can help you to familiarize yourself with the concept of a dataset.
