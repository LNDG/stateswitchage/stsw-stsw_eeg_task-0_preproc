#!/bin/bash

# This script prepares tardis by compiling the necessary function in MATLAB.

#ssh tardis # access tardis

# check and choose matlab version
#module avail matlab
module load matlab/R2016b

# compile functions

matlab
%% add fieldtrip toolbox
addpath('/home/mpib/LNDG/StateSwitch/WIP_eeg/SA_preproc_study_YA/T_tools/fieldtrip-20170904/')
ft_defaults()
ft_compile_mex(true)
%% go to analysis directory containing .m-file
cd('/home/mpib/LNDG/StateSwitch/WIP_eeg/SA_preproc_study_YA/A_scripts/G_STSW_segmentation/')
%% compile function and append dependencies
mcc -m G_STSW_segmentation_raw_data_180111.m -a /home/mpib/LNDG/StateSwitch/WIP_eeg/SA_preproc_study_YA/A_scripts/helper