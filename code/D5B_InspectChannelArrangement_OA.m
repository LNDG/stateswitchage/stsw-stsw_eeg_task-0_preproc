pn.study        = '/Volumes/LNDG/Projects/StateSwitch/';
pn.eeg_root     = [pn.study, 'dynamic/data/eeg/task/A_preproc/SA_preproc_study/'];
pn.EEG          = [pn.eeg_root, 'B_data/C_EEG_FT/'];
pn.History      = [pn.eeg_root, 'B_data/D_History/'];
pn.scripts      = [pn.eeg_root, 'A_scripts']; addpath(pn.scripts);
pn.figures      = [pn.eeg_root, 'C_figures/C_ChannelCheck/']; mkdir(pn.figures);
% add ConMemEEG tools
pn.MWBtools     = [pn.eeg_root, 'T_tools/fnct_MWB/'];            addpath(genpath(pn.MWBtools));
pn.THGtools     = [pn.eeg_root, 'T_tools/fnct_THG/'];            addpath(genpath(pn.THGtools));
pn.commontools  = [pn.eeg_root, 'T_tools/fnct_common/'];         addpath(genpath(pn.commontools));
pn.fnct_JQK     = [pn.eeg_root, 'T_tools/fnct_JQK/'];           addpath(genpath(pn.fnct_JQK));
pn.FT           = [pn.eeg_root, 'T_tools/fieldtrip-20170904/']; addpath(pn.FT); ft_defaults;

%% define IDs for visual screening

% N = 53 OAs;
IDs = {'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';'2252';'2258';'2261'};

for id = 1:numel(IDs)
    disp(num2str(id))
    data_EEG = [];
    load([pn.EEG, num2str(IDs{id}), '_r1_dynamic_EEG_Raw_Rlm_Flh_Res.mat'],'data_EEG')
    tmp = corrcoef(data_EEG.trial{1}');
    Rmat(id,1:66,1:66) = tmp(1:66,1:66);
end

h = figure;
for indID = 1:size(Rmat,1)
    subplot(7,8,indID);
    imagesc(squeeze(Rmat(indID,:,:)));
    title(IDs{indID});
end
pn.plotFolder = pn.figures;
figureName = 'ChannelCheck_correlation_OA';
saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');
