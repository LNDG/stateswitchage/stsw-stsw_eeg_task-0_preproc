%% Import EyeEEG Data

% 170913 | JQK adapted function from MD's scripts
% 171221 | JQK adapted for study STSWD task
% 180103 | JQK had to change triggers to range from 1 to 64 vs 2 to 128 due
%           to recreated triggers in ET data
%        | offset marker set to 128
%        | exclude subject 1228 run 4
% 180205 | adjusted for OAs
% 180206 | for 2160, initial trigger is missing, excluded for now
%        | for 2203, initial trigger is missing, excluded for now

%% initialize

clear; close all; pack; clc;

%% load in path & toolboxes

pn.study    = '/Volumes/LNDG/Projects/StateSwitch/';
pn.eye_root = [pn.study, 'dynamic/data/eye/SA_EEG/'];
pn.eeg_root = [pn.study, 'dynamic/data/eeg/task/A_preproc/SA_preproc_study/'];
pn.eye_IN   = [pn.eye_root, 'A_preprocessing/B_data/D_eye_mat_edited/'];
pn.eeg_IN   = [pn.eeg_root, 'B_data/A_raw/'];
pn.data_OUT = [pn.eeg_root, 'B_data/B_EEG_ET_ByRun/']; mkdir(pn.data_OUT);
pn.logs     = [pn.eeg_root, 'Y_logs/A_EEG_ET_ByRun_logs/']; mkdir(pn.logs);
pn.eeglab   = [pn.eeg_root, 'T_Tools/eeglab14_1_1b/']; addpath(pn.eeglab);
pn.fnct_JQK = [pn.eeg_root, 'T_Tools/fnct_JQK/']; addpath(genpath(pn.fnct_JQK));
pn.plotFolder = [pn.eeg_root, 'C_figures/A_EEG_ET_ByRun_Figures/']; mkdir(pn.plotFolder);

%% open eeglab

eeglab % use eegh to get history of EEGlab commands!

%% datadef

condEEG = 'dynamic'; % Note: ET data were only recorded during the task.

%% define IDs for preprocessing

% % N = 48;
% IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';'1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1213';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};

% N = 53 OAs;
IDs = {'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';'2252';'2258';'2261'};

%% merge EEG & ET data

for indID = 1:length(IDs)
    
    % Define ID
    ID = IDs{indID};
    
    % create log
    diary([pn.logs, num2str(ID) ,'_', condEEG , '_ET_EEG_notes_',date,'.txt']);
    
    %% load raw EEG data by run
    
    eegIn = ['ss_',num2str(ID), '_dynamic.vhdr'];
    mrkIn = ['ss_',num2str(ID), '_dynamic.vmrk'];
    % load markers
    mrk = bva_readmarker([pn.eeg_IN, mrkIn]);
    % if multiple marker files are available: concatenate them
    if iscell(mrk)
        mrk = cat(2,mrk{:});
    end
    % find run on/offset
    onsets = find(mrk(1,:)== 1); % first onset marker
    offsets = find(mrk(1,:)== 128); % final offset marker [set to 128]
    if strcmp(num2str(ID), '2160') || strcmp(num2str(ID), '2203')
        onsets = [1 onsets];
    end
    % check whether any on/offset markers are incorrect (e.g. restarted presentation)
    excludeOnsets = find(diff(onsets)<1000);
    excludeOffsets = find(diff(offsets)<1000);
    if ~isempty(excludeOnsets)
        onsets(excludeOnsets) = [];
    end
    if ~isempty(excludeOffsets)
        offsets(excludeOffsets) = [];
    end
    tmp_onsetMrk = mrk(:,onsets);
    if strcmp(IDs{indID}, '1228') % subject 1228 had only 3 runs recorded
        tmp_onsetMrk(:,4) = [];
    end
    tmp_offsetMrk = mrk(:,offsets);
    SegmentMat = [tmp_onsetMrk(2,:)', tmp_offsetMrk(2,:)'];
    fprintf(['Run length is: ', num2str(diff(SegmentMat, [], 2)'), '\n']);
    % load by run
    for run = 1:4
        if strcmp(IDs{indID}, '1228') & run == 4
            continue
        end
        EEG = pop_loadbv(pn.eeg_IN, eegIn, SegmentMat(run,:));
        [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 0,'setname','data_EEG','gui','off');
        EEGByRun{run} = EEG;
    end; clear EEG;
        
    %% merge ET & EEG data by run
    
    for run = 1:4
        if strcmp(IDs{indID}, '1228') & run == 4
            continue
        elseif strcmp(IDs{indID}, '2160') & run == 1
            continue
        elseif strcmp(IDs{indID}, '2203') & run == 1
            continue
        end
        % set paths
        eyeIn = [pn.eye_IN, 'S', num2str(ID), 'r', num2str(run), '.mat'];
        dataOut_eegET = [pn.data_OUT, num2str(ID), '_r', num2str(run), '_', condEEG, '_eyeEEG'];
        dataOut_mrk = [pn.data_OUT, num2str(ID), '_r', num2str(run), '_', condEEG, '_mrk_eyeEEG.mat'];
        % check whether files have already been created
        if 1 %~exist(dataOut_mrk, 'file')
            fprintf(['... Merging ET & EEG \n']);
            % set beginning/offset markers
            triggers = [1 64]; % second onset trigger, second-to-last offset trigger
            % merge data
            EEG = EEGByRun{run};
            EEG = eeg_checkset(EEG);
            if exist(eyeIn) % only add ET data if it exists
                EEG = pop_importeyetracker(EEG,eyeIn,triggers ,[1:9] ,{'TIME' 'L_GAZE_X' 'L_GAZE_Y' 'L_AREA' 'L_VEL_X' 'L_VEL_Y' 'RES_X' 'RES_Y' 'INPUT'},1,1,0,0);
            end
%             saveas(gcf, [pn.plotFolder, num2str(ID), '_r', num2str(run), '_', condEEG, '_eyeEEGSync'], 'fig');
%             saveas(gcf, [pn.plotFolder, num2str(ID), '_r', num2str(run), '_', condEEG, '_eyeEEGSync'], 'epsc');
            [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 2,'setname','data_EYE_EEG','savenew',dataOut_eegET,'gui','off');
            % save mrk/event files
            event = EEG.event;
            urevent = EEG.urevent;
            save(dataOut_mrk, 'event', 'urevent')
        else
            fprintf(['... Merging already done. Proceeding with next run/subject. \n']);
        end
    end
    diary off
end


% 
% %%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %% read in data
% 
% cfg = [];
% cfg.dataset = inputdlg('Please enter the path to the eye-eeg dataset'); % note: use .set file, not .fdt!
% dataset_orig = cfg.dataset;
% data_eyeeeg = ft_preprocessing(cfg);
% 
% %% read in events
% 
% event_eyeeeg = data_eyeeeg.hdr.orig{1,1}.orig.event;    % use events from eeglab?
% unique({event_eyeeeg.type})
% 
% for indID = 1:length(event_eyeeeg)
%     event_eyeeeg(1,indID).sample = event_eyeeeg(1,indID).latency;
%     event_eyeeeg(1,indID).value  = 1;
% end;
% 
% 
% %% plot data
% cfg = [];
% 
% cfg.continuous     = 'yes';
% cfg.viewmode       = 'vertical';
% cfg.preproc.demean = 'yes';
% channel_selection = data_eyeeeg.hdr.label;
% 
% %select relevant channels
% for indID = 10:65
%     channel_selection{indID,1} = [];
% end;
% A = cellfun(@isempty, channel_selection);
% channel_selection = channel_selection(A~=1,1);
% cfg.channel       = channel_selection;
% 
% cfg.event          = event_eyeeeg;
% cfg.ylim           = [-5000, 5000];                     % cfg.ylim           = ['ymin ymax]'; vs = 'maxmin';
% cfg.blocksize      = 10;
% cfg.plotlabels     = 'yes';
% cfg.colorgroups    = 'chantype';
% 
% cfg = ft_databrowser(cfg,data_eyeeeg);
% 
